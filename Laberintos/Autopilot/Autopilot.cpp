#include "Autopilot.h"
#include "../Utils/BitUtils.h"
#include <EEPROM.h>
#define DEBUG_ENABLED false

Autopilot::Autopilot(uint8_t _mazeDimension, uint8_t _mazeXFinish,
		uint8_t _mazeYFinish, uint8_t _mazeFinishSize, bool _rightHandRule,
		bool _enableFinishSpecialSearch) {
	mazeDimension = _mazeDimension;
	data = calloc(mazeDimension * mazeDimension, sizeof(uint8_t));
	mazeXFinish = _mazeXFinish;
	mazeYFinish = _mazeYFinish;
	mazeFinishSize = _mazeFinishSize;
	rightHandRule = _rightHandRule;
	enableFinishSpecialSearch = _enableFinishSpecialSearch;
	lastRobotOrientation = NORTH;
	robotXPos = 0;
	robotYPos = 0;
	oldRobotXPos = 0;
	oldRobotYPos = 0;

}

Autopilot::~Autopilot() {
	free(data);
	data = NULL;
}

void Autopilot::setCurrentPositionData(bool front, bool left, bool right,
bool back) {
	bool first = !getBit(data[robotXPos * mazeDimension + robotYPos],
	VISITED_CELL);

	bool north;
	bool south;
	bool east;
	bool west;
	switch (lastRobotOrientation) {
	case NORTH:
		north = front;
		south = first ?
				true :
				getBit(data[robotXPos * mazeDimension + robotYPos], SOUTH_BIT);
		east = right;
		west = left;

		break;
	case SOUTH:
		north = first ?
				true :
				getBit(data[robotXPos * mazeDimension + robotYPos], NORTH_BIT);
		south = front;
		east = left;
		west = right;
		break;
	case EAST:
		north = left;
		south = right;
		east = front;
		west = first ?
				true :
				getBit(data[robotXPos * mazeDimension + robotYPos], WEST_BIT);
		break;
	case WEST:
		north = right;
		south = left;
		east = first ?
				true :
				getBit(data[robotXPos * mazeDimension + robotYPos], EAST_BIT);
		west = front;
		break;
	}

	//delay(100);
	if (DEBUG_ENABLED) {
		Serial.print("set N ");
		Serial.print((north << NORTH_BIT));
		Serial.print(" e ");
		Serial.print(east);
		Serial.print(" ");
	}

	uint8_t byte = (north << NORTH_BIT) + (south << SOUTH_BIT)
			+ (east << EAST_BIT) + (west << WEST_BIT) + (1 << VISITED_CELL)
			+ (1 << BEST_PATH_CELL);
	if (DEBUG_ENABLED) {
		Serial.println(byte, HEX);
	}

	data[robotXPos * mazeDimension + robotYPos] = byte;
	//printArray();

}

Instruction Autopilot::getInstructionAndUpdatePosition(bool learningMode) {
	Instruction instruction;

	if (lastInstruction == TURN_RIGHT || lastInstruction == TURN_LEFT
			|| lastInstruction == TURN_180) {
		instruction = GO_STRAIGHT;
	} else {
		bool north = getBit(data[robotXPos * mazeDimension + robotYPos],
		NORTH_BIT);
		bool south = getBit(data[robotXPos * mazeDimension + robotYPos],
		SOUTH_BIT);
		bool east = getBit(data[robotXPos * mazeDimension + robotYPos],
		EAST_BIT);
		bool west = getBit(data[robotXPos * mazeDimension + robotYPos],
		WEST_BIT);

		bool front;
		bool left;
		bool right;
		bool back;

		//adaptar orientación.
		switch (lastRobotOrientation) {
		case NORTH:
			front = north;
			back = south;
			left = west;
			right = east;
			break;
		case SOUTH:
			back = north;
			front = south;
			right = west;
			left = east;

			break;
		case EAST:
			right = south;
			left = north;
			front = east;
			back = west;
			break;
		case WEST:
			left = south;
			right = north;
			back = east;
			front = west;
			break;
		}
		if (learningMode) {
			//calcular movemento.
			//se podemos entrar a meta ignoramos o resto.
			if (enableFinishSpecialSearch && left
					&& isThereFinishInRelativeOrientationForRobot(WEST)) {
				instruction = TURN_LEFT;
			} else if (enableFinishSpecialSearch && right
					&& isThereFinishInRelativeOrientationForRobot(EAST)) {
				instruction = TURN_RIGHT;
			} else if (enableFinishSpecialSearch && front
					&& isThereFinishInRelativeOrientationForRobot(NORTH)) {
				instruction = GO_STRAIGHT;
			} else if (rightHandRule && right) {
				instruction = TURN_RIGHT;
			} else if (!rightHandRule && left) {
				instruction = TURN_LEFT;
			} else {
				if (front) {
					instruction = GO_STRAIGHT;
				} else if (left) {
					instruction = TURN_LEFT;
				} else if (right) {
					instruction = TURN_RIGHT;
				} else if (back) {
					instruction = TURN_180;
				} else {
					//TODO
				}
			}
		} else {
			if (front
					&& isBestRoute(
							getNextOrientation(lastRobotOrientation,
									GO_STRAIGHT))) {
//				Serial.println("vamos diante");
				instruction = GO_STRAIGHT;
			} else if (left
					&& isBestRoute(
							getNextOrientation(lastRobotOrientation,
									TURN_LEFT))) {
//				Serial.println("vamos esquerda");
				instruction = TURN_LEFT;
			} else if (right
					&& isBestRoute(
							getNextOrientation(lastRobotOrientation,
									TURN_RIGHT))) {
//				Serial.println("vamos dereita");
				instruction = TURN_RIGHT;
			}
		}
	}

	oldRobotXPos = robotXPos;
	oldRobotYPos = robotYPos;

	if (instruction == GO_STRAIGHT) {

		//actualizar posición mapa.
		setRobotNewPosition(lastRobotOrientation);
		//si la posición a la que vamos ya la hemos visitado, se entiende que es forzado y marcamos la actual como mala
		if (learningMode && getBit(data[robotXPos * mazeDimension + robotYPos],
		VISITED_CELL)) {
			setBit(&data[oldRobotXPos * mazeDimension + oldRobotYPos],
			BEST_PATH_CELL, 0);
			if (DEBUG_ENABLED) {
				Serial.println("MARCO POS actual como mala");
			}
		}
	} else {
		lastRobotOrientation = getNextOrientation(lastRobotOrientation,
				instruction);
	}

	lastInstruction = instruction;
	if (DEBUG_ENABLED) {
		Serial.print("estoy en X: ");
		Serial.print(oldRobotXPos);
		Serial.print(" Y: ");
		Serial.print(oldRobotYPos);
		Serial.print(" voy a X: ");
		Serial.print(robotXPos);
		Serial.print(" Y: ");
		Serial.print(robotYPos);
		Serial.print(" lastRobotOrientation: ");
		Serial.println(lastRobotOrientation);
	}

	return instruction;
}

Orientation Autopilot::getNextOrientation(Orientation oldOrientation,
		Instruction instruction) {
	Orientation orientation;
	if (instruction != GO_STRAIGHT) {
		switch (oldOrientation) {
		case NORTH:
			if (instruction == TURN_LEFT) {
				orientation = WEST;
			} else if (instruction == TURN_RIGHT) {
				orientation = EAST;
			} else if (instruction == TURN_180) {
				orientation = SOUTH;
			}
			break;
		case SOUTH:
			if (instruction == TURN_LEFT) {
				orientation = EAST;
			} else if (instruction == TURN_RIGHT) {
				orientation = WEST;
			} else if (instruction == TURN_180) {
				orientation = NORTH;
			}
			break;
		case EAST:
			if (instruction == TURN_LEFT) {
				orientation = NORTH;
			} else if (instruction == TURN_RIGHT) {
				orientation = SOUTH;
			} else if (instruction == TURN_180) {
				orientation = WEST;
			}
			break;
		case WEST:
			if (instruction == TURN_LEFT) {
				orientation = SOUTH;
			} else if (instruction == TURN_RIGHT) {
				orientation = NORTH;
			} else if (instruction == TURN_180) {
				orientation = EAST;
			}
			break;
		}
	} else {
		orientation = oldOrientation;
	}
	return orientation;
}

void Autopilot::setRobotNewPosition(Orientation orientation) {

	switch (orientation) {
	case NORTH:
		robotYPos++;
		break;
	case SOUTH:
		robotYPos--;
		break;
	case EAST:
		robotXPos++;
		break;
	case WEST:
		robotXPos--;
		break;
	}
	robotXPos %= mazeDimension;
	robotYPos %= mazeDimension;
}
bool Autopilot::isThereFinishInRelativeOrientationForRobot(
		Orientation orientation) {
	uint8_t possibleNextXPos = robotXPos;
	uint8_t possibleNextYPos = robotYPos;
	//TODO check desbordamentos (<0)
	switch (lastRobotOrientation) {
	case NORTH:
		switch (orientation) {
		case NORTH:
			possibleNextYPos++;
			break;
		case EAST:
			possibleNextXPos++;
			break;
		case WEST:
			possibleNextXPos--;
			break;
		}
		break;
	case SOUTH:
		switch (orientation) {
		case NORTH:
			possibleNextYPos--;
			break;
		case EAST:
			possibleNextXPos--;
			break;
		case WEST:
			possibleNextXPos++;
			break;
		}
		break;
	case EAST:
		switch (orientation) {
		case NORTH:
			possibleNextXPos++;
			break;
		case EAST:
			possibleNextYPos--;
			break;
		case WEST:
			possibleNextYPos++;
			break;
		}
		break;
	case WEST:
		switch (orientation) {
		case NORTH:
			possibleNextXPos--;
			break;
		case EAST:
			possibleNextYPos++;
			break;
		case WEST:
			possibleNextYPos--;
			break;
		}
		break;
	}

	possibleNextXPos %= mazeDimension;
	possibleNextYPos %= mazeDimension;
	if ((possibleNextXPos == mazeXFinish
			|| possibleNextXPos == (mazeXFinish + mazeFinishSize))
			&& (possibleNextYPos == mazeYFinish
					|| possibleNextYPos == (mazeYFinish + mazeFinishSize))) {
		Serial.print("atajo meta desde ");
		Serial.print(robotXPos);
		Serial.print(" ");
		Serial.println(robotYPos);
		return true;
	}
//	Serial.print("no atajo meta desde ");
//	Serial.print(robotXPos);
//	Serial.print(" ");
//	Serial.print(robotYPos);
//	Serial.print(" a ");
//	Serial.print(possibleNextXPos);
//	Serial.print(" ");
//	Serial.print(possibleNextYPos);
//	Serial.print(" meta ");
//	Serial.print(mazeXFinish);
//	Serial.print(" ");
//	Serial.println(mazeYFinish);
	return false;
}

bool Autopilot::isBestRoute(Orientation orientation) {
	uint8_t newXPos = robotXPos;
	uint8_t newYPos = robotYPos;
	switch (orientation) {
	case NORTH:
		newYPos++;
		break;
	case SOUTH:
		newYPos--;
		break;
	case EAST:
		newXPos++;
		break;
	case WEST:
		newXPos--;
		break;
	}
	newXPos %= mazeDimension;
	newYPos %= mazeDimension;
	bool isBest = getBit(data[newXPos * mazeDimension + newYPos],
	BEST_PATH_CELL);
	if (DEBUG_ENABLED) {
		Serial.print("isBest ");
		Serial.print(newXPos);
		Serial.print(" ");
		Serial.print(newYPos);
		Serial.print(" ");
		Serial.println(isBest);
	}
	return isBest;
}

bool Autopilot::hasToTurn(bool getRight) {
	if (!getRight
			&& isBestRoute(
					getNextOrientation(lastRobotOrientation, TURN_LEFT))) {
		return true;
	} else if (getRight
			&& isBestRoute(
					getNextOrientation(lastRobotOrientation, TURN_RIGHT))) {
		return true;
	}
	return false;
}

bool Autopilot::wasWallInPreviousPosition(bool getRight) {
	uint8_t oldDataPosition = data[oldRobotXPos * mazeDimension + oldRobotYPos];
	bool north = getBit(oldDataPosition,
	NORTH_BIT);
	bool south = getBit(oldDataPosition,
	SOUTH_BIT);
	bool east = getBit(oldDataPosition,
	EAST_BIT);
	bool west = getBit(oldDataPosition,
	WEST_BIT);

	bool left;
	bool right;

	//adaptar orientación.
	switch (lastRobotOrientation) {
	case NORTH:
		left = west;
		right = east;
		break;
	case SOUTH:
		right = west;
		left = east;
		break;
	case EAST:
		right = south;
		left = north;
		break;
	case WEST:
		left = south;
		right = north;
		break;
	}
	return getRight ? !right : !left;
}

void Autopilot::loadData() {
	for (int i = 0; i < mazeDimension * mazeDimension; i += sizeof(data[0])) {
		data[i] = EEPROM.read(i);
		Serial.print(data[i], HEX);
	}
	Serial.println("LEIDO");
}

void Autopilot::printArray() {
	for (int i = 0; i < mazeDimension * mazeDimension; i++) {
		Serial.print(data[i], HEX);
	}
	Serial.println();
}

void Autopilot::saveData() {
	for (int i = 0; i < mazeDimension * mazeDimension; i += sizeof(data[0])) {
		EEPROM.write(i, data[i]);
		Serial.print(data[i], HEX);
	}
	Serial.println("ESCRITO");
}
