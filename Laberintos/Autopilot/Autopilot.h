#include <Arduino.h>

#ifndef AUTOPILOT_AUTOPILOT_H_
#define AUTOPILOT_AUTOPILOT_H_

//bits codificados en matriz de mapa
#define NORTH_BIT 0
#define SOUTH_BIT 1
#define EAST_BIT 2
#define WEST_BIT 3
#define VISITED_CELL 4
#define BEST_PATH_CELL 5
//libres bit 6-7

enum Orientation {
	NORTH, SOUTH, EAST, WEST
};
enum Instruction {
	GO_STRAIGHT, TURN_LEFT, TURN_RIGHT, TURN_180
};

class Autopilot {
public:
	~Autopilot();
	Autopilot(uint8_t _mazeDimension, uint8_t _mazeXFinish,
			uint8_t _mazeYFinish, uint8_t _mazeFinishSize, bool _rightHandRule,
			bool _enableFinishSpecialSearch);
	/**booleanos que indican true = libre, false = pared **/
	void setCurrentPositionData(bool front, bool left, bool right, bool back);
	Instruction getInstructionAndUpdatePosition(bool learningMode);
	void setFinish();
	void saveData();
	void loadData();

	bool wasWallInPreviousPosition(bool getRight);

	bool hasToTurn(bool getRight);

private:
	Orientation getNextOrientation(Orientation oldOrientation,
			Instruction instruction);

	void setRobotNewPosition(Orientation oldOrientation);

	bool isBestRoute(
			Orientation orientation);

	void printArray();

	bool isThereFinishInRelativeOrientationForRobot(Orientation orientation);

	bool rightHandRule;
	bool enableFinishSpecialSearch;

	uint8_t mazeDimension;
	uint8_t mazeXFinish;
	uint8_t mazeYFinish;
	uint8_t mazeFinishSize;
	uint8_t *data;
	uint8_t robotXPos;
	uint8_t robotYPos;
	uint8_t oldRobotXPos;
	uint8_t oldRobotYPos;

	Orientation lastRobotOrientation;
	Instruction lastInstruction;
};

#endif
