
#ifndef GAPDETECTOR_GAP_H_
#define GAPDETECTOR_GAP_H_

#define NO_GAP 0b0
#define FRONT_GAP 0b1
#define FRONT_STOP_GAP 0b10
#define LEFT_GAP 0b100
#define RIGHT_GAP 0b1000
#define FRONT_LEFT_GAP 0b10000
#define FRONT_RIGHT_GAP 0b100000
#define REAR_LEFT_GAP 0b1000000
#define REAR_RIGHT_GAP 0b10000000


#endif
