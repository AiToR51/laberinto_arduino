#include "GapDetector.h"
#include <Wire.h>

#define SIDE_FACTOR 0.5
#define ADDRESS_RIGHT_LASER 0b0111001

GapDetector::GapDetector(unsigned int frontLeftPin, unsigned int frontRightPin,
		unsigned int frontPin, unsigned int rearLeftPin,
		unsigned int rearRightPin, unsigned int rearPin,
		unsigned int sideThreshold, unsigned int frontalThreshold,
		unsigned int frontalStopThreshold, bool useRear) {
	_frontLeftPin = frontLeftPin;
	_frontRightPin = frontRightPin;
	_frontPin = frontPin;
	_rearLeftPin = rearLeftPin;
	_rearRightPin = rearRightPin;
	_rearPin = rearPin;
	_sideThreshold = sideThreshold;
	_frontalThreshold = frontalThreshold;
	_frontalStopThreshold = frontalStopThreshold;

	_frontLeftInitialRead = 0;
	_frontRightInitialRead = 0;
	_frontInitialRead = 0;
	_rearLeftInitialRead = 0;
	_rearRightInitialRead = 0;
	_rearInitialRead = 0;
	_useLaser = false;
	_useRear = useRear;
}

GapDetector::GapDetector(unsigned int frontPin,
		unsigned int leftLaserShutdownPin, unsigned int sideThreshold,
		unsigned int frontalThreshold, unsigned int frontalStopThreshold) {
	_frontPin = frontPin;
	_useLaser = true;
	_useRear = false;
	_sideThreshold = sideThreshold;
	_frontalThreshold = frontalThreshold;
	_frontalStopThreshold = frontalStopThreshold;
	_leftLaserShutdownPin = leftLaserShutdownPin;
//	pinMode(leftLaserShutdownPin, OUTPUT);

}

GapDetector::~GapDetector() {

}

void GapDetector::calibrate() {
	if (_useLaser) {
		Wire.begin();
		Serial.println("calibrating");
		digitalWrite(_leftLaserShutdownPin, LOW);
		delay(10);
		_rightLaserSensor = new VL6180X();
//		if (!_rightLaserSensor->begin()) {
//			Serial.println("Failed to find sensor right");
//		} else {
//			Serial.println("right ok");
//		}

		_rightLaserSensor->init();
		_rightLaserSensor->configureDefault();
		_rightLaserSensor->setAddress(ADDRESS_RIGHT_LASER);
		delay(10);
		digitalWrite(_leftLaserShutdownPin, HIGH);
		_leftLaserSensor = new VL6180X();
//		if (!_leftLaserSensor->begin()) {
//			Serial.println("Failed to find sensor left");
//		} else {
//			Serial.println("left ok");
//		}
		_leftLaserSensor->init();
		_leftLaserSensor->configureDefault();
		_leftLaserSensor->setScaling(2);
		_rightLaserSensor->setScaling(2);
		_leftLaserSensor->startRangeContinuous(20);
		_rightLaserSensor->startRangeContinuous(20);
		Serial.println("calibrating - mes");

	}

	for (int i = 0; i < CALIBRATE_TIMES; i++) {
		updateReadsInternal();
		_frontLeftInitialRead += _frontTempLeftRead;
		_frontRightInitialRead += _frontTempRightRead;
		_frontInitialRead += _frontTempRead;
		_rearLeftInitialRead += _rearTempLeftRead;
		_rearRightInitialRead += _rearTempRightRead;
		_rearInitialRead += _rearTempRead;
		delay(100);
	}
	_frontLeftInitialRead /= CALIBRATE_TIMES;
	_frontRightInitialRead /= CALIBRATE_TIMES;
	_frontInitialRead /= CALIBRATE_TIMES;
	_rearLeftInitialRead /= CALIBRATE_TIMES;
	_rearRightInitialRead /= CALIBRATE_TIMES;
//	_rearInitialRead /= CALIBRATE_TIMES;

	Serial.print("_frontLeftInitialRead ");
	Serial.print(_frontLeftInitialRead);

	Serial.print(" _frontRightInitialRead ");
	Serial.print(_frontRightInitialRead);

	Serial.print(" _frontInitialRead ");
	Serial.print(_frontInitialRead);
	Serial.print(" _rearLeftInitialRead ");
	Serial.print(_rearLeftInitialRead);

	Serial.print(" _rearRightInitialRead ");
	Serial.print(_rearRightInitialRead);
	Serial.println();

	if (_useLaser) {
		_frontLeftThreshold = _frontLeftInitialRead + 70;
		_frontRightThreshold = _frontRightInitialRead + 70;
	} else {
		_frontLeftThreshold = _frontLeftInitialRead * SIDE_FACTOR;
		_frontRightThreshold = _frontRightInitialRead * SIDE_FACTOR;
		if (_useRear) {
			_rearLeftThreshold = _rearLeftInitialRead * SIDE_FACTOR;
			_rearRightThreshold = _rearRightInitialRead * SIDE_FACTOR;
		} else {
			_rearLeftThreshold = _frontLeftThreshold;
			_rearRightThreshold = _frontRightThreshold;
		}
	}
	_frontThreshold = _frontalThreshold;
//	_rearThreshold = _frontThreshold;
}

void GapDetector::updateReads() {
	_frontLeftRead = 0;
	_frontRightRead = 0;
	_frontRead = 0;
	_rearLeftRead = 0;
	_rearRightRead = 0;
	for (int i = 0; i < READ_COUNT; i++) {
		updateReadsInternal();
		_frontLeftRead += _frontTempLeftRead;
		_frontRightRead += _frontTempRightRead;
		_frontRead += _frontTempRead;
		_rearLeftRead += _rearTempLeftRead;
		_rearRightRead += _rearTempRightRead;
//		_rearRead += _rearRead;
	}
	_frontLeftRead /= READ_COUNT;
	_frontRightRead /= READ_COUNT;
	_frontRead /= READ_COUNT;
	_rearLeftRead /= READ_COUNT;
	_rearRightRead /= READ_COUNT;
}

void GapDetector::updateReadsInternal() {
	_frontTempRead = analogRead(_frontPin);

	if (!_useLaser) {
		_frontTempLeftRead = analogRead(_frontLeftPin);
		_frontTempRightRead = analogRead(_frontRightPin);
		if (_useRear) {
			_rearTempLeftRead = analogRead(_rearLeftPin);
			_rearTempRightRead = analogRead(_rearRightPin);
		} else {
			_rearTempLeftRead = _frontTempLeftRead;
			_rearTempRightRead = _frontTempRightRead;
		}
//	_rearTempRead = analogRead(_rearPin);
	} else {
//		Serial.println("mesRIGHT");
		_frontTempRightRead =
				_rightLaserSensor->readRangeContinuousMillimeters();
//				_rightLaserSensor->readRangeContinuousMillimeters();
//		Serial.println("mesLEFT");

		_frontTempLeftRead = _leftLaserSensor->readRangeContinuousMillimeters();
		//leer de l�ser. (sin bloqueo?)
	}
}

uint8_t GapDetector::getCurrentGaps(boolean printDebug) {
	updateReads();
	bool frontLeft;
	bool frontRight;

	bool rearLeft;
	bool rearRight;
	bool rear;
	bool front = _frontRead < _frontThreshold;
	bool frontStop = _frontRead < _frontalStopThreshold;

	if (_useLaser) {
		frontLeft = _frontLeftRead > _frontLeftThreshold;
		frontRight = _frontRightRead > _frontRightThreshold;
		rearLeft = frontLeft;
		rearRight = frontRight;
	} else {
		frontLeft = _frontLeftRead < _frontLeftThreshold;
		frontRight = _frontRightRead < _frontRightThreshold;
		rearLeft = _rearLeftRead < _rearLeftThreshold;
		rearRight = _rearRightRead < _rearRightThreshold;
		rear = _rearRead < _rearThreshold;
	}

	if (printDebug) {

		Serial.print("frontLeftRead ");
		Serial.print(_frontLeftRead);
		Serial.print(" / ");
		Serial.print(_frontLeftThreshold);
		Serial.print(" : ");
		Serial.println(getFrontLeftDiff());

		Serial.print("frontRightRead ");
		Serial.print(_frontRightRead);
		Serial.print(" / ");
		Serial.print(_frontRightThreshold);
		Serial.print(" : ");
		Serial.println(getFrontRightDiff());

//		Serial.print("rearLeftRead ");
//		Serial.print(_rearLeftRead);
//		Serial.print(" / ");
//		Serial.print(_rearLeftThreshold);
//		Serial.print(" : ");
//		Serial.println(getRearLeftDiff());
//
//		Serial.print("rearRightRead ");
//		Serial.print(_rearRightRead);
//		Serial.print(" / ");
//		Serial.print(_rearRightThreshold);
//		Serial.print(" : ");
//		Serial.println(getRearRightDiff());

		Serial.print("frontRead ");
		Serial.print(_frontRead);
		Serial.print(" / ");
		Serial.println(_frontThreshold);
		Serial.println();

		/**/
	}

	uint8_t returnValue = NO_GAP;

	if (front) {
		returnValue |= FRONT_GAP;
	}
	if (frontStop) {
		returnValue |= FRONT_STOP_GAP;
	}
	if (frontLeft) {
		returnValue |= FRONT_LEFT_GAP;
	}
	if (rearLeft) {
		returnValue |= REAR_LEFT_GAP;
	}
	if (frontLeft && rearLeft) {
		returnValue |= LEFT_GAP;
	}

	if (frontRight) {
		returnValue |= FRONT_RIGHT_GAP;
	}
	if (rearRight) {
		returnValue |= REAR_RIGHT_GAP;
	}
	if (frontRight && rearRight) {
		returnValue |= RIGHT_GAP;
	}

	return returnValue;
}

int GapDetector::calcPonderation(int value, int maxAbsValue,
		int maxAbsPonderation) {
	if (value >= maxAbsValue) {
		return maxAbsPonderation;
	} else if (value <= -maxAbsValue) {
		return -maxAbsPonderation;
	} else {
		return (value * maxAbsPonderation) / maxAbsValue;
	}
}

#define MAX_DEVIATION_INSIDE_CELL 50

#define VALUE_PONDERATION_MAX 10
//devuelve un numero entre -10 y 10, siendo -10 maximo desvio a la izquierda y 10 a derecha. 0 centrado
int8_t GapDetector::getCenterDeviationInfo(boolean useRightNotLeft) {
	updateReads();
	if (!_useLaser) {
		int8_t frontDeviation = calcPonderation(
				useRightNotLeft ? getFrontRightDiff() : -1 * getFrontLeftDiff(),
				MAX_DEVIATION_INSIDE_CELL, VALUE_PONDERATION_MAX);
		int8_t rearDeviation = calcPonderation(
				useRightNotLeft ? getRearRightDiff() : -1 * getRearLeftDiff(),
				MAX_DEVIATION_INSIDE_CELL, VALUE_PONDERATION_MAX);

		return (frontDeviation + rearDeviation) / 2;
	} else {
		int8_t frontDeviation = calcPonderation(
				useRightNotLeft ? -1 * getFrontRightDiff() : getFrontLeftDiff(),
				MAX_DEVIATION_INSIDE_CELL, VALUE_PONDERATION_MAX);
		return frontDeviation;
	}

}

//TODO devolver un indice entre -10 y 10, indicando -10 maxima inclinacion hacia izquierda y 10 hacia derecha. 0 centrado
int8_t GapDetector::getSideInfo(boolean useRightNotLeft) {
	updateReads();
	if (!_useLaser) {
		return calcPonderation(
				useRightNotLeft ?
						getFrontRightDiff() - getRearRightDiff() :
						getRearLeftDiff() - getFrontLeftDiff(),
				MAX_DEVIATION_INSIDE_CELL, VALUE_PONDERATION_MAX);
	} else {
		//TODO usar gir�scopo.
		return 0;
	}
}

int GapDetector::getSideMMDiff(long j) {
	if (_useLaser) {
		return j;
	} else {
		double d = ((j * j * j) / 1177020.0) - (j * j) / 1628.0 + 0.23 * j
				+ 0.89;
		return (int) d;
	}
}

int GapDetector::getFrontLeftDiff() {
	return getSideMMDiff(_frontLeftRead - _frontLeftInitialRead);
}

int GapDetector::getFrontRightDiff() {
	return getSideMMDiff(_frontRightRead - _frontRightInitialRead);
}

int GapDetector::getRearLeftDiff() {
	return getSideMMDiff(_rearLeftRead - _rearLeftInitialRead);
}

int GapDetector::getRearRightDiff() {
	return getSideMMDiff(_rearRightRead - _rearRightInitialRead);
}
