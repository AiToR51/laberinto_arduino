
#ifndef GAPDETECTOR_GAPDETECTOR_H_
#define GAPDETECTOR_GAPDETECTOR_H_
#include <Arduino.h>
#include "Gap.h"
#include "../VL6180X/VL6180X.h"
//#include "../Adafruit_VL6180X/Adafruit_VL6180X.h"

#define CALIBRATE_TIMES 5
#define READ_COUNT 1

class GapDetector {
public:
	~GapDetector();
	GapDetector(unsigned int frontPin, unsigned int leftLaserShutdownPin,
			unsigned int sideThreshold, unsigned int frontalThreshold,
			unsigned int frontalStopThreshold);
	GapDetector(unsigned int frontLeftPin, unsigned int frontRightPin, unsigned int frontPin,
			unsigned int rearLeftPin, unsigned int rearRightPin, unsigned int rearPin,
			unsigned int sideThreshold, unsigned int frontalThreshold,
			unsigned int frontalStopThreshold, bool useRear);
	void calibrate();
	uint8_t getCurrentGaps(boolean printDebug);
	int8_t getSideInfo(boolean useRightNotLeft);
	int8_t getCenterDeviationInfo(boolean useRightNotLeft);
	void updateReads();
private:
	unsigned int _frontLeftPin;
	unsigned int _frontRightPin;
	unsigned int _frontPin;
	unsigned int _rearLeftPin;
	unsigned int _rearRightPin;
	unsigned int _rearPin;
	unsigned int _sideThreshold;
	unsigned int _frontalThreshold;
	unsigned int _frontalStopThreshold;
	unsigned int _leftLaserShutdownPin;

	bool _useLaser;
	bool _useRear;

	int _frontLeftInitialRead;
	int _frontRightInitialRead;
	int _frontInitialRead;
	int _rearLeftInitialRead;
	int _rearRightInitialRead;
	int _rearInitialRead;

	int _frontLeftRead;
	int _frontRightRead;
	int _frontRead;
	int _rearLeftRead;
	int _rearRightRead;
	int _rearRead;

	int _frontTempLeftRead;
	int _frontTempRightRead;
	int _frontTempRead;
	int _rearTempLeftRead;
	int _rearTempRightRead;
	int _rearTempRead;

	int _frontLeftThreshold;
	int _frontRightThreshold;
	int _frontThreshold;
	int _rearLeftThreshold;
	int _rearRightThreshold;
	int _rearThreshold;

	VL6180X* _leftLaserSensor;
	VL6180X* _rightLaserSensor;

	int getSideMMDiff(long j);
	int getFrontLeftDiff();
	int getFrontRightDiff();
	int getRearLeftDiff();
	int getRearRightDiff();
	int calcPonderation(int value, int maxAbsValue, int maxAbsPonderation);
	void updateReadsInternal();

};
#endif
