#include <Arduino.h>
#include <PID_v1.h>
#include "Motors/Motors.h"
#include "GapDetector/GapDetector.h"
#include "Autopilot/Autopilot.h"
#include "config/SpeedProfile.h"
#include "config/BasicExploreSpeedProfile.h"
#include "config/BasicExploreContinuousSpeedProfile.h"
#include "config/BasicGOSpeedProfile.h"
#include "config/BasicGOFastSpeedProfile.h"

#include <PushButton.h>

//BT pass 1232

#define MOTOR_L_1 10
#define MOTOR_L_2 11
#define MOTOR_R_1 9
#define MOTOR_R_2 6
#define MOTOR_L_INTERRUPT_PIN 2
#define MOTOR_R_INTERRUPT_PIN 3

#define INPUT_OPERATION_MODE_PIN 4
#define INPUT_BUTTON_PIN 5

#define INPUT_MODE_0_PIN 7
#define INPUT_MODE_1_PIN 8

//#define SHUTDOWN_RIGHT_LASER_SENSOR_PIN 12

#define FRONT_ANALOG_SENSOR_PIN 0
#define FRONT_LEFT_ANALOG_SENSOR_PIN 2
#define FRONT_RIGHT_ANALOG_SENSOR_PIN 1
#define REAR_LEFT_ANALOG_SENSOR_PIN 3
#define REAR_RIGHT_ANALOG_SENSOR_PIN 6

#define FLOOR_FINISH_LINE_SENSOR 7
#define SIDE_ANALOG_SENSORS_THRESHOLD 150
#define SIDE_LASER_SENSORS_THRESHOLD 150

#define FRONTAL_SENSORS_THRESHOLD 260
#define FRONTAL_STOP_THRESHOLD 445

#define FLOOR_SENSOR_THRESHOLD 0.65

#define PULSES_BY_CM 25//23
#define PULSES_AFTER_INTERSECTION PULSES_BY_CM*9//03 originalmente, ahora 9 porque asumimos que s�lo el morro del robot asom� en la intersecci�n. Y qudar�a medio camino.
#define MAZE_SQUARE_PULSES PULSES_BY_CM*18
#define TURN_180_DEGREE_PULSES 320
#define TURN_90_DEGREE_PULSES 150

#define USE_CORRECTIONS true
#define USE_MOTORS true
#define PID_OUTPUT_MAX_VALUE 50

#define DEBUG_ENABLED true

#define MAZE_CELLS_X 16
#define ENABLE_MAZE_FINISH_SPECIAL_SEARCH false //si puede entrar a meta auqnue no deba por regla iz/dcha entra.  TODO revisar lo del inicio en sentido horario oshwdem para poder CENTRARSE
#define MAZE_FINISH_X 7
#define MAZE_FINISH_Y 7
#define MAZE_FINISH_RAD 1//1 = x+1, y+1, 0 = meta de 1 casilla

//oshwdem = 7,7,1
//casa 2,1,0

#define MAX_EXECUTIONS 5;//maximo numero d ecasillas. 0 = infinito. SOLO TEST XXX
bool rightHand;

bool resetPID = false;
bool comingIntoTurn = false;

bool fromContinuous = false;

bool learningMode;

bool enableContinuousMode;

enum State {
	GOING_FORWARD, TURNING_LEFT, TURNING_RIGHT, TURNING_180, STOPPED, FINISH
};

Motors motors = Motors(MOTOR_L_1, MOTOR_L_2, 2, MOTOR_R_1, MOTOR_R_2, 3);
GapDetector gapDetector = GapDetector(FRONT_LEFT_ANALOG_SENSOR_PIN,
FRONT_RIGHT_ANALOG_SENSOR_PIN, FRONT_ANALOG_SENSOR_PIN,
REAR_LEFT_ANALOG_SENSOR_PIN, REAR_RIGHT_ANALOG_SENSOR_PIN, -1,
SIDE_ANALOG_SENSORS_THRESHOLD,
FRONTAL_SENSORS_THRESHOLD, FRONTAL_STOP_THRESHOLD, false);

//GapDetector gapDetector = GapDetector(FRONT_ANALOG_SENSOR_PIN,
//		SHUTDOWN_RIGHT_LASER_SENSOR_PIN,
//SIDE_LASER_SENSORS_THRESHOLD, FRONTAL_SENSORS_THRESHOLD,
//		FRONTAL_STOP_THRESHOLD);

Autopilot *autopilot;
SpeedProfile *speedProfile;

double setPoint, input, pidOutput;

PID *pidController;

State currentState = STOPPED;
State previousState = STOPPED;

boolean runningNewState;

unsigned long pulseAfterIntersectionCount = 0L;

int floorThreshold;

boolean inFinish = false;
unsigned long currentTargetPulses;
Pushbutton inputButton(INPUT_BUTTON_PIN, PULL_UP_DISABLED, DEFAULT_STATE_LOW);

int cycleCount = 0;

void printInterruptionsData() {
	noInterrupts();
	Serial.print("L motor ");
	Serial.println(motors.getMotorLInterruptCount());
	Serial.print("R motor ");
	Serial.println(motors.getMotorRInterruptCount());
	Serial.println();
	//motors.resetMotorLInterruptCount();
	//motors.resetMotorRInterruptCount();
	interrupts();
}

void interruptedLMotor() {
	motors.interruptedLMotor();
}

void interruptedRMotor() {
	motors.interruptedRMotor();
}

// datos lab oshwdem _frontLeftInitialRead 352 _frontRightInitialRead 347 _frontInitialRead 86 _rearLeftInitialRead 323 _rearRightInitialRead 338

int8_t calcPIDCorrections(boolean leftGap, boolean rightGap) {
	int8_t centeredValue = 0;
	int8_t centeredValueL = 0;
	int8_t centeredValueR = 0;
	int8_t sideInfo = 0;
	int8_t sideInfoL = 0;
	int8_t sideInfoR = 0;
	int8_t powerDiff = 0;

	if (USE_CORRECTIONS && (!rightGap || !leftGap)) {
		//digitalWrite(13, HIGH);

		// negativo izquierda, positivo dereita
		centeredValueL = gapDetector.getCenterDeviationInfo(
		false);
		centeredValueR = gapDetector.getCenterDeviationInfo(
		true);

		if (!rightGap && !leftGap) {
			centeredValue =
			abs(centeredValueL) > abs(centeredValueR) ?
					centeredValueL : centeredValueR;
		} else if (rightGap) {
			centeredValue = centeredValueL;
		} else {
			centeredValue = centeredValueR;
		}

		sideInfoL = gapDetector.getSideInfo(false);
		sideInfoR = gapDetector.getSideInfo(true);

		if (!rightGap && !leftGap) {
			if (centeredValue > 0) {
				sideInfo = sideInfoR;
			} else {
				sideInfo = sideInfoL;
			}
		} else if (rightGap) {
			sideInfo = sideInfoL;
		} else {
			sideInfo = sideInfoR;
		}

		//						sideInfo = gapDetector.getSideInfo(!rightGap); //FIXMe revisar esto que � feo.
		input = centeredValue;// + 2 * sideInfo; //FIXMe si se deja de usar laser

		if (resetPID) {
			pidController->outputSum = 0.0;
//			pidController->.ITerm = 0.0;

			pidController->lastInput = input;
			resetPID = false;
		}
		pidController->Compute();
		powerDiff = pidOutput;

//		Serial.print("sideInfo ");
//		Serial.print(sideInfo);
//		Serial.print(", centeredValueL ");
//		Serial.print(centeredValueL);
//		Serial.print(", centeredValueR ");
//		Serial.print(centeredValueR);
//		Serial.print(", centeredValue ");
//		Serial.print(centeredValue);
//		Serial.print(", PID input ");
//		Serial.print(input);
//		Serial.print(", out ");
//		Serial.println(powerDiff);
	}
	return powerDiff;
}

int getFloorSensorRead() {
	return (analogRead(FLOOR_FINISH_LINE_SENSOR)
			+ analogRead(FLOOR_FINISH_LINE_SENSOR)) / 2;
}

void debugGaps() {
	gapDetector.calibrate();
	while (true) {
		gapDetector.getCurrentGaps(true);
		delay(500);
	}
}

void checkMotors() {
	delay(1000);
	while (true) {
//		for (int i = 35; i <= 200; i += 15) {
//			if (i > 99 && i < 116) {
//				i = 140;
//			}
		for (int j = 0; j < 1; j++) {
			motors.goFront(65, 65);
			delay(2000);
//				motors.stop();
			Serial.print("potencia ");
			Serial.println(50);
			printInterruptionsData();
			motors.resetMotorLInterruptCount();
			motors.resetMotorRInterruptCount();

			motors.stop();
			delay(1500);
		}
//		}

		/*motors.goFront(50, 50);
		 delay(100);
		 motors.goFront(220, 220);
		 delay(2000);
		 motors.stop();*/
		motors.stop();
		delay(5000);
		/*motors.goFront(50, 50);

		 motors.stop();
		 printInterruptionsData();
		 motors.resetMotorLInterruptCount();
		 motors.resetMotorRInterruptCount();
		 delay(2000);*/
	}
}

void changeState(State newState) {
	digitalWrite(13, LOW);

	if (!(GOING_FORWARD == newState && fromContinuous)) {
		motors.stop();
	}
	previousState = currentState;
	currentState = newState;

	currentTargetPulses = MAZE_SQUARE_PULSES;
	if (currentState == TURNING_LEFT || currentState == TURNING_RIGHT) {
		currentTargetPulses = TURN_90_DEGREE_PULSES;
	} else if (currentState == TURNING_180) {
		currentTargetPulses = TURN_180_DEGREE_PULSES;
	}

	runningNewState = false;
//	printInterruptionsData();
	delay(10);
	motors.resetMotorLInterruptCount();
	motors.resetMotorRInterruptCount();
	pulseAfterIntersectionCount = 0L;
	//pidController.Initialize();
	resetPID = true;
	comingIntoTurn = false;
}

void printGap(uint8_t gap) {
	if ((gap & FRONT_GAP) > 0) {
		Serial.println("FRONT_GAP");
	}
	if ((gap & FRONT_STOP_GAP) > 0) {
		Serial.println("FRONT_STOP_GAP");
	}
	if ((gap & LEFT_GAP) > 0) {
		Serial.println("LEFT_GAP");
	}
	if ((gap & RIGHT_GAP) > 0) {
		Serial.println("RIGHT_GAP");
	}
	if ((gap & FRONT_LEFT_GAP) > 0) {
		Serial.println("FRONT_LEFT_GAP");
	}
	if ((gap & FRONT_RIGHT_GAP) > 0) {
		Serial.println("FRONT_RIGHT_GAP");
	}

	if ((gap & REAR_LEFT_GAP) > 0) {
		Serial.println("REAR_LEFT_GAP");
	}
	if ((gap & REAR_RIGHT_GAP) > 0) {
		Serial.println("REAR_RIGHT_GAP");
	}
}

void changeState() {
	uint8_t gap = gapDetector.getCurrentGaps(DEBUG_ENABLED);
//	printGap(gap);

	if (learningMode) {
		autopilot->setCurrentPositionData((gap & FRONT_GAP) > 0,
				(gap & LEFT_GAP) > 0, (gap & RIGHT_GAP) > 0, true);
	}
	if (inFinish) {
		changeState(FINISH);
		if (learningMode) {
			autopilot->saveData();
		}
		return;
	}
	Instruction instruction = autopilot->getInstructionAndUpdatePosition(
			learningMode);

	switch (instruction) {
	case GO_STRAIGHT:
		if (DEBUG_ENABLED) {
			Serial.println("changeState - GOING_FORWARD");
		}
		changeState(GOING_FORWARD);

		break;
	case TURN_LEFT:
		if (DEBUG_ENABLED) {
			Serial.println("changeState - TURNING_LEFT");
		}
		changeState(TURNING_LEFT);
		break;
	case TURN_RIGHT:
		if (DEBUG_ENABLED) {
			Serial.println("changeState - TURNING_RIGHT");
		}
		changeState(TURNING_RIGHT);
		break;
	case TURN_180:
		if (DEBUG_ENABLED) {
			Serial.println("changeState - TURNING_180");
		}
		changeState(TURNING_180);
		break;
	}
	if (learningMode && !fromContinuous) {
		delay(20);
	}
//	delay(100);
}

boolean hasToTurn(boolean right) {
	if (learningMode) {
		return (right && rightHand) || (!right && !rightHand);
	} else {
		return autopilot->hasToTurn(right);
	}
}

byte getPowerFront(boolean frontGap, boolean stopFrontGap, boolean fullRightGap,
		boolean fullLeftGap, boolean frontRightGap, boolean frontLeftGap,
		boolean previousRightWall, boolean previousLeftWall) {

	unsigned long currentAVGInterruptCount = motors.getAVGInterruptCount();

	int percentDone = 0;
	if (currentTargetPulses == currentAVGInterruptCount) { //TODO >????
		percentDone = 100;
	} else {
		percentDone = (currentAVGInterruptCount * 100) / currentTargetPulses;
	}

	boolean possibleContinue = false;
	if (percentDone > 55) {
		if (learningMode) {
			possibleContinue = speedProfile->isEnabledContinuousMode()
					&& !(frontLeftGap && hasToTurn(false))
					&& !(frontRightGap && hasToTurn(true) && frontGap);
		} else {
			possibleContinue = speedProfile->isEnabledContinuousMode()
					&& !(hasToTurn(false)) && !(hasToTurn(true) && frontGap);
		}
		if (possibleContinue && DEBUG_ENABLED) {
			//Serial.println("continuar recto");
		}
	}

	byte newPower = speedProfile->getFrontSpeed(percentDone, fromContinuous,
			possibleContinue);

	if (percentDone > 50) {
		/*Serial.print("previousRightWall ");
		 Serial.print(previousRightWall);
		 Serial.print("fullRightGap ");
		 Serial.print(fullRightGap);
		 Serial.print("hasToTurn(true) ");
		 Serial.println(hasToTurn(true));

		 */
		if (!stopFrontGap) {
			if (DEBUG_ENABLED) {
				Serial.println("STOP por frente");
			}
			newPower = 0;
		} else if (newPower == 0 && !frontGap) {
			if (DEBUG_ENABLED) {
				Serial.println("CONT por aprox a stop");
			}
			newPower = speedProfile->getFrontSpeed(percentDone, false, 95);
		} else if (percentDone > 50
				&& ((previousRightWall && fullRightGap && hasToTurn(true))
						|| (previousLeftWall && fullLeftGap && hasToTurn(false)))) {
			//con s�lo 2 sensores dejamos esto en 50%, con los 4 se usaba 65 porque es cunado el robot pasaba
			if (!comingIntoTurn) {
				if (DEBUG_ENABLED) {
					Serial.print("-- interseccion ori: ");
					Serial.print(currentTargetPulses);
					Serial.print(" new ");
					Serial.println(
					PULSES_AFTER_INTERSECTION + currentAVGInterruptCount);
				}
				comingIntoTurn = true;
				currentTargetPulses = PULSES_AFTER_INTERSECTION
						+ currentAVGInterruptCount;
			}
		}
	}

	return newPower;
}

void doCalcs() {
	if (currentState != STOPPED) {
		if (!runningNewState) {
			runningNewState = true;
			if (USE_MOTORS) {
				if (currentState == GOING_FORWARD) {
					motors.goFront(35, 35);
				} else if (currentState == TURNING_LEFT) {
					motors.turn(false, 35);
				} else if (currentState == TURNING_RIGHT) {
					motors.turn(true, 35);
				} else if (currentState == TURNING_180) {
					motors.turn(true, 35);
				}
			}
		} else {
			//1 - comprobamos GAPs para os lados, para saber se estamos chegando ou salindo dunha interseccion

			boolean rightGap = false;
			boolean leftGap = false;
			uint8_t gap = gapDetector.getCurrentGaps(false);
			if ((gap & FRONT_RIGHT_GAP) > 0 || (gap & REAR_RIGHT_GAP) > 0) {
				rightGap = true;
			}
			if ((gap & FRONT_LEFT_GAP) > 0 || (gap & REAR_LEFT_GAP) > 0) {
				leftGap = true;
			}
			bool fullRightGap = (gap & RIGHT_GAP) > 0;
			bool fullLeftGap = (gap & LEFT_GAP) > 0;
			bool frontRightGap = (gap & FRONT_RIGHT_GAP) > 0;
			bool frontLeftGap = (gap & FRONT_LEFT_GAP) > 0;
			bool previousLeftWall = autopilot->wasWallInPreviousPosition(
			false);
			bool previousRightWall = autopilot->wasWallInPreviousPosition(true);
			bool frontGap = (gap & FRONT_GAP) > 0;

			//2 - comprobamos paralelismo solo si dun lado hai parede. - SIDE info + posicion
			//3 - en caso de poder correxir paralelo, faise no novo movemento cun diferencial de potencias entre rodas.
			unsigned long currentAVGInterruptCount =
					motors.getAVGInterruptCount();

			//	Serial.println(currentAVGInterruptCount);
			byte newPower = 30;

			boolean stop = false;

			if (currentState == TURNING_LEFT || currentState == TURNING_RIGHT
					|| currentState == TURNING_180) {
				if (currentTargetPulses >= currentAVGInterruptCount) {
					int percentDone = 0;
					if (currentTargetPulses == currentAVGInterruptCount) {
						percentDone = 100;
					} else {
						percentDone = (currentAVGInterruptCount * 100)
								/ currentTargetPulses;
					}

					newPower = speedProfile->getTurnSpeed(percentDone);
					if (newPower == 0) {
						stop = true;
					}
					if (USE_MOTORS && !stop) {
						if (currentState == TURNING_LEFT) {
							motors.turn(false, newPower);
						} else if (currentState == TURNING_RIGHT) {
							motors.turn(true, newPower);
						} else if (currentState == TURNING_180) {
							motors.turn(true, newPower);
						}
					}

				} else {
					stop = true;
				}
			} else if (currentState == GOING_FORWARD) {
				int8_t powerDiff = calcPIDCorrections(leftGap, rightGap);
				newPower = getPowerFront(frontGap, gap & FRONT_STOP_GAP,
						fullRightGap, fullLeftGap, frontRightGap, frontLeftGap,
						previousRightWall, previousLeftWall);
				if (newPower == 0) {
					stop = true;
				} else {
					powerDiff = (newPower * powerDiff) / 100;
					if (USE_MOTORS) {
						motors.goFront(newPower + powerDiff,
								newPower - powerDiff);
					}
				}
			}

			if (stop && USE_MOTORS) {
				if (enableContinuousMode && !(fullRightGap && hasToTurn(true))
						&& !(fullLeftGap && hasToTurn(false)) && frontGap
						&& GOING_FORWARD == currentState) {
					Serial.println("Cambiando al vueloo");
					fromContinuous = true;
					changeState();
				} else {
					motors.stop();
					delay(50);
//					motors.goRear(50);
//					delay(70);
//					motors.stop();
					fromContinuous = false;
					changeState(STOPPED);
					delay(100);
				}

			}
		}
	}
}

void tempInfo(boolean useRight) {

	int centeredValue = gapDetector.getCenterDeviationInfo(useRight);
	int sideInfo = gapDetector.getSideInfo(useRight);
	Serial.print("sideInfo ");
	Serial.print(sideInfo);
	Serial.print(", centeredValue ");
	Serial.println(centeredValue);
}

void setup() {
	Serial.begin(115200);
	delay(100);
	rightHand = !inputButton.isPressed();
	if (!rightHand) {
		digitalWrite(13, HIGH);
		delay(500);
		digitalWrite(13, LOW);
		delay(500);
		digitalWrite(13, HIGH);
		delay(500);
		digitalWrite(13, LOW);
		delay(2000);
	}

	pinMode(MOTOR_L_INTERRUPT_PIN, INPUT_PULLUP);
	pinMode(MOTOR_R_INTERRUPT_PIN, INPUT_PULLUP);
	pinMode(13, OUTPUT);
	pinMode(MOTOR_L_1, OUTPUT);
	pinMode(MOTOR_L_2, OUTPUT);
	pinMode(MOTOR_R_1, OUTPUT);
	pinMode(MOTOR_R_2, OUTPUT);
	pinMode(INPUT_OPERATION_MODE_PIN, INPUT_PULLUP);
	pinMode(INPUT_MODE_0_PIN, INPUT_PULLUP);
	pinMode(INPUT_MODE_1_PIN, INPUT_PULLUP);

//	pinMode(SHUTDOWN_RIGHT_LASER_SENSOR_PIN, OUTPUT);

	pinMode(INPUT_BUTTON_PIN, INPUT);

	attachInterrupt(digitalPinToInterrupt(MOTOR_L_INTERRUPT_PIN),
			interruptedLMotor,
			RISING);
	attachInterrupt(digitalPinToInterrupt(MOTOR_R_INTERRUPT_PIN),
			interruptedRMotor,
			RISING);
	digitalWrite(13, HIGH);
//	digitalWrite(SHUTDOWN_RIGHT_LASER_SENSOR_PIN, HIGH);

	autopilot = new Autopilot(MAZE_CELLS_X, MAZE_FINISH_X, MAZE_FINISH_Y,
	MAZE_FINISH_RAD, rightHand, ENABLE_MAZE_FINISH_SPECIAL_SEARCH);

	learningMode = digitalRead(INPUT_OPERATION_MODE_PIN);

	boolean mode0 = digitalRead(INPUT_MODE_0_PIN);
	boolean mode1 = digitalRead(INPUT_MODE_1_PIN);

	if (DEBUG_ENABLED) {
		Serial.print("learning ");
		Serial.print(learningMode);
		Serial.print(" mode: ");
		Serial.print(mode0);
		Serial.println(mode1);
	}
//	debugGaps();
//	checkMotors();
	if (!mode0 && !mode1 && learningMode) {
		inputButton.waitForPress();
		Serial.println("MODO TEST motores");
		//checkMotors();
	}
	if (learningMode) {
		if (!mode1) {
			speedProfile = new BasicExploreContinuousSpeedProfile();
		} else {
			speedProfile = new BasicExploreSpeedProfile();
		}
	} else {
		if (!mode1) {
			speedProfile = new BasicGOFastSpeedProfile();
		} else {
			speedProfile = new BasicGOSpeedProfile();
		}
	}

//	Serial.print("Speed auto? ");
//	Serial.print(speedProfile->isEnabledContinuousMode());
//	Serial.print(" front 50 ");
//	Serial.println(speedProfile->getFrontSpeed(50, false, false));

	enableContinuousMode = speedProfile->isEnabledContinuousMode();
	if (!learningMode) {
		autopilot->loadData();
//		enableContinuousMode = true;
	}

	pidController = new PID(&input, &pidOutput, &setPoint,
			speedProfile->getPIDKp(), speedProfile->getPIDKi(),
			speedProfile->getPIDKd(), DIRECT);
	delay(100);
	inputButton.waitForPress();

	delay(2000);
	gapDetector.calibrate();
//	while (true) {
//		printGap(gapDetector.getCurrentGaps(true));
////		tempInfo(true);
////		tempInfo(false);
////		Serial.println("------");
//		delay(1000);
//	}
	floorThreshold = getFloorSensorRead() * FLOOR_SENSOR_THRESHOLD;
//	while (true) {
//		Serial.println(getFloorSensorRead());
//		delay(100);
//	}

	digitalWrite(13, LOW);
	setPoint = 0;
	pidController->SetOutputLimits(-PID_OUTPUT_MAX_VALUE, PID_OUTPUT_MAX_VALUE);
	pidController->SetSampleTime(20);
	pidController->SetMode(AUTOMATIC);

	inputButton.waitForPress();
	delay(2000);
}

void loop() {

	if (currentState == STOPPED) {
		changeState();
	}
	if (currentState != FINISH) {
		doCalcs();
	}

	if (!inFinish && getFloorSensorRead() < floorThreshold) {
		inFinish = true;
		if (DEBUG_ENABLED) {
			Serial.println("META");
		}
	}
	delay(5);
//tempInfo();

}

