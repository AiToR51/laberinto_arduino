#include "Motors.h"

#include <stdlib.h>
#include <Arduino.h>


Motors::Motors(unsigned int motorL1Pin, unsigned int motorL2Pin,
		unsigned int motorLInterruptPin, unsigned int motorR1Pin,
		unsigned int motorR2Pin, unsigned int motorRInterruptPin) {
	_motorL1Pin = motorL1Pin;
	_motorL2Pin = motorL2Pin;
	_motorLInterruptPin = motorLInterruptPin;
	_motorR1Pin = motorR1Pin;
	_motorR2Pin = motorR2Pin;
	_motorRInterruptPin = motorRInterruptPin;
	_motorLInterruptCount = 0;
	_motorRInterruptCount = 0;
	pinMode(_motorL1Pin, OUTPUT);
	pinMode(_motorL2Pin, OUTPUT);
	pinMode(_motorR1Pin, OUTPUT);
	pinMode(_motorR2Pin, OUTPUT);
}

byte calcCorrectedPower(byte rightPower) {
	if (rightPower < 51) {
		return rightPower * 0.94;
	} else if (rightPower < 70) {
		return rightPower * 0.88;
	} else if (rightPower < 100) {
		return rightPower * 0.9;
	} else if (rightPower < 150) {
		return rightPower * 0.94;
	} else if (rightPower < 165) {
		return rightPower * 0.98;
	} else if (rightPower < 200) {
		return rightPower * 1.01;
	} else if (rightPower < 240) {
		return rightPower * 1.03;
	} else {
		return max(255, rightPower * 1.04);
	}
	return rightPower;
}


void Motors::goFront(byte leftPower, byte rightPower) {
	analogWrite(_motorL2Pin, 0);
	analogWrite(_motorR2Pin, 0);
	analogWrite(_motorR1Pin, calcCorrectedPower(rightPower));
	analogWrite(_motorL1Pin, leftPower);
}

void Motors::goRear(byte power) {
	analogWrite(_motorL1Pin, 0);
	analogWrite(_motorR1Pin, 0);
	analogWrite(_motorL2Pin, power);
	analogWrite(_motorR2Pin, calcCorrectedPower(power));
}

void Motors::stop() {
//	analogWrite(_motorL1Pin, 0);
//	analogWrite(_motorR1Pin, 0);
//	analogWrite(_motorL2Pin, 0);
//	analogWrite(_motorR2Pin, 0);
	digitalWrite(_motorL1Pin, HIGH);
	digitalWrite(_motorR1Pin, HIGH);
	digitalWrite(_motorL2Pin, HIGH);
	digitalWrite(_motorR2Pin, HIGH);
}

void Motors::turn(boolean right, byte power) {
	if (right) {
		digitalWrite(_motorL2Pin, LOW);
		digitalWrite(_motorR1Pin, LOW);
		analogWrite(_motorL1Pin, power);
		analogWrite(_motorR2Pin, calcCorrectedPower(power));
	} else {
		digitalWrite(_motorL1Pin, LOW);
		digitalWrite(_motorR2Pin, LOW);
		analogWrite(_motorL2Pin, power);
		analogWrite(_motorR1Pin, calcCorrectedPower(power));
	}
}

unsigned long Motors::getMotorLInterruptCount() {
	return _motorLInterruptCount;
}

unsigned long Motors::getMotorRInterruptCount() {
	return _motorRInterruptCount;
}

unsigned long Motors::getAVGInterruptCount() {
	return (_motorLInterruptCount + _motorRInterruptCount) / 2;
}

void Motors::resetMotorLInterruptCount() {
	noInterrupts();
	_motorLInterruptCount = 0L;
	interrupts();
}

void Motors::resetMotorRInterruptCount() {
	noInterrupts();
	_motorRInterruptCount = 0L;
	interrupts();
}

void Motors::interruptedLMotor() {
	_motorLInterruptCount++;
}

void Motors::interruptedRMotor() {
	_motorRInterruptCount++;
}

// the destructor frees up allocated memory
Motors::~Motors() {

}
