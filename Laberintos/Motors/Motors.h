
#ifndef Motors_h
#define Motors_h

#include <Arduino.h>


class Motors {
public:
	~Motors();
	Motors(unsigned int motorL1Pin, unsigned int motorL2Pin, unsigned int motorLInterruptPin, unsigned int motorR1Pin, unsigned int motorR2Pin, unsigned int motorRInterruptPin);
	void goFront(byte leftPower, byte rightPower);
	void goRear(byte power);
	void turn(boolean right, byte power);
	void stop();
	unsigned long getMotorLInterruptCount();
	unsigned long getMotorRInterruptCount();
	unsigned long getAVGInterruptCount();
	void resetMotorLInterruptCount();
	void resetMotorRInterruptCount();
	void interruptedLMotor();
	void interruptedRMotor();
private:
	unsigned int _motorL1Pin;
	unsigned int _motorL2Pin;
	unsigned int _motorLInterruptPin;
	unsigned int _motorR1Pin;
	unsigned int _motorR2Pin;
	unsigned int _motorRInterruptPin;
	unsigned long _motorLInterruptCount;
	unsigned long _motorRInterruptCount;
};

#endif
