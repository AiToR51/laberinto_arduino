/*
 * States.h
 *
 *  Created on: 14 oct. 2016
 *      Author: ATR
 */

#ifndef STATES_H_
#define STATES_H_

#define STOPPED 0
#define GOING_FORWARD 1
#define TURNING_LEFT 2
#define TURNING_RIGHT 3


#endif /* STATES_H_ */
