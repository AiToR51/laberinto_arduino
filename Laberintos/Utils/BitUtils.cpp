#include "BitUtils.h"

bool getBit(uint8_t word, int position) {
	return 1 & (word >> position);
}

void setBit(uint8_t* word, int position, boolean bitValue) {
	if (bitValue) {
		*word|= 1 << position;
	} else {
		*word &= ~(1 << position);
	}
}
