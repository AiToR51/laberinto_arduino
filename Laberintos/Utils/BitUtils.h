
#ifndef UTILS_BITUTILS_H_
#define UTILS_BITUTILS_H_
#include <Arduino.h>

bool getBit(uint8_t word, int position);

void setBit(uint8_t* word, int position, boolean bitValue);

#endif
