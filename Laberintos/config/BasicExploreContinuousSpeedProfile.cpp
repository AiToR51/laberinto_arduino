#include "BasicExploreContinuousSpeedProfile.h"

bool BasicExploreContinuousSpeedProfile::isEnabledContinuousMode() {
	return true;
}

byte BasicExploreContinuousSpeedProfile::getFrontSpeed(int percentDone,
bool fromContinuous, bool continueStraight) {
	byte speed = 0;
	if (percentDone < 15 && !fromContinuous) {
		speed = 35;
	} else if (percentDone < 60 || (continueStraight && percentDone < 100)) {
		speed = 45;
	} else if (percentDone < 85) {
		speed = 45;
	} else if (percentDone < 100) {
		speed = 30;
	} else {
		if (DEBUG_ENABLED) {
			Serial.println("fin por 100%");
		}
		speed = 0;
	}
	return speed;
}

double BasicExploreContinuousSpeedProfile::getPIDKp() {
	return 5.0;
}

double BasicExploreContinuousSpeedProfile::getPIDKi() {
	return 0.5;
}

double BasicExploreContinuousSpeedProfile::getPIDKd() {
	return 1.0;
}
