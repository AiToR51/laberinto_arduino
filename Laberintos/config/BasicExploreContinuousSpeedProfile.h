/*
 * profile.h
 *
 *  Created on: 8 nov. 2017
 *      Author: AGR
 */

#ifndef CONFIG_BASICEXPLORECONTSPEEDPROFILE_H_
#define CONFIG_BASICEXPLORECONTSPEEDPROFILE_H_

#include <Arduino.h>
#include "BasicExploreSpeedProfile.h"

class BasicExploreContinuousSpeedProfile: public BasicExploreSpeedProfile {
public:
	BasicExploreContinuousSpeedProfile() :
			BasicExploreSpeedProfile() {
	}
	;bool isEnabledContinuousMode();
	byte getFrontSpeed(int percentDone, bool fromContinuous,
			bool continueStraight);

	double getPIDKp();
	double getPIDKi();
	double getPIDKd();
private:
};

#endif /* CONFIG_BASICEXPLORECONTSPEEDPROFILE_H_ */
