#include "BasicExploreSpeedProfile.h"

BasicExploreSpeedProfile::BasicExploreSpeedProfile() {

}

BasicExploreSpeedProfile::~BasicExploreSpeedProfile() {

}

double BasicExploreSpeedProfile::getPIDKp() {
	return 5.0;
}

double BasicExploreSpeedProfile::getPIDKi() {
	return 0.5;
}

double BasicExploreSpeedProfile::getPIDKd() {
	return 1.0;
}

byte BasicExploreSpeedProfile::get180TurnSpeed(int percentDone) {
	byte speed = 0;
	if (percentDone < 15) {
		speed = 35;
	} else if (percentDone < 40) {
		speed = 50;
	} else if (percentDone < 70) {
		speed = 60;
	} else if (percentDone < 90) {
		speed = 40;
	} else if (percentDone < 100) {
		speed = 30;
	}
	return speed;
}

byte BasicExploreSpeedProfile::getTurnSpeed(int percentDone) {
	byte speed = 0;
	if (percentDone < 15) {
		speed = 35;
	} else if (percentDone < 40) {
		speed = 50;
	} else if (percentDone < 70) {
		speed = 60;
	} else if (percentDone < 90) {
		speed = 40;
	} else if (percentDone < 100) {
		speed = 30;
	}
	return speed;
}

byte BasicExploreSpeedProfile::getFrontSpeed(int percentDone,
bool fromContinuous, bool continueStraight) {
	byte speed = 0;
	if (percentDone < 15) {
		speed = 35;
	} else if (percentDone < 60) {
		speed = 50;
	} else if (percentDone < 85) {
		speed = 45;
	} else if (percentDone < 100) {
		speed = 35;
	} else {
		if (DEBUG_ENABLED) {
			Serial.println("fin por 100%");
		}
		speed = 0;
	}
	return speed;
}

bool BasicExploreSpeedProfile::isEnabledContinuousMode() {
	return false;
}
