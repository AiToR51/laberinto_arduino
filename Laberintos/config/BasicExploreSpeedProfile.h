/*
 * profile.h
 *
 *  Created on: 8 nov. 2017
 *      Author: AGR
 */

#ifndef CONFIG_BASICEXPLORESPEEDPROFILE_H_
#define CONFIG_BASICEXPLORESPEEDPROFILE_H_

#include <Arduino.h>
#include "SpeedProfile.h"

class BasicExploreSpeedProfile: public SpeedProfile {
public:
	BasicExploreSpeedProfile();
	~BasicExploreSpeedProfile();bool isEnabledContinuousMode();
	byte getTurnSpeed(int percentDone);
	byte get180TurnSpeed(int percentDone);
	/**
	 * continueStraight indica si es posible que haya que seguir recto */
	byte getFrontSpeed(int percentDone, bool fromContinuous,
	bool continueStraight);

	double getPIDKp();
	double getPIDKi();
	double getPIDKd();
private:
};

#endif /* CONFIG_BASICEXPLORESPEEDPROFILE_H_ */
