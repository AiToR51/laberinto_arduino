#include "BasicGOFastSpeedProfile.h"

BasicGOFastSpeedProfile::BasicGOFastSpeedProfile() {

}

BasicGOFastSpeedProfile::~BasicGOFastSpeedProfile() {

}

byte BasicGOFastSpeedProfile::get180TurnSpeed(int percentDone) {
	byte speed = 0;
	if (percentDone < 15) {
		speed = 35;
	} else if (percentDone < 40) {
		speed = 60;
	} else if (percentDone < 70) {
		speed = 75;
	} else if (percentDone < 90) {
		speed = 40;
	} else if (percentDone < 100) {
		speed = 30;
	}
	return speed;
}

byte BasicGOFastSpeedProfile::getTurnSpeed(int percentDone) {
	byte speed = 0;
	if (percentDone < 15) {
		speed = 35;
	} else if (percentDone < 40) {
		speed = 60;
	} else if (percentDone < 70) {
		speed = 75;
	} else if (percentDone < 90) {
		speed = 40;
	} else if (percentDone < 100) {
		speed = 30;
	}
	return speed;
}

byte BasicGOFastSpeedProfile::getFrontSpeed(int percentDone,
bool fromContinuous, bool continueStraight) {
	byte speed = 0;
	if (percentDone < 15 && !(fromContinuous && isEnabledContinuousMode())) {
		speed = 40;
	} else if (percentDone < 30
			&& !(fromContinuous && isEnabledContinuousMode())) {
		speed = 60;
	} else if (percentDone < 60
			|| (continueStraight && isEnabledContinuousMode()
					&& percentDone < 100)) {
		speed = 75;
	} else if (percentDone < 85) {
		speed = 55;
	} else if (percentDone < 100) {
		speed = 35;
	} else {
		if (DEBUG_ENABLED) {
			Serial.println("fin por 100%");
		}
		speed = 0;
	}
	return speed;
}

bool BasicGOFastSpeedProfile::isEnabledContinuousMode() {
	return true;
}

double BasicGOFastSpeedProfile::getPIDKp() {
	return 5.0;
}

double BasicGOFastSpeedProfile::getPIDKi() {
	return 0.5;
}

double BasicGOFastSpeedProfile::getPIDKd() {
	return 1.0;
}
