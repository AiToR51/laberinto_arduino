/*
 * profile.h
 *
 *  Created on: 8 nov. 2017
 *      Author: AGR
 */

#ifndef CONFIG_BASICGOFASTSPEEDPROFILE_H_
#define CONFIG_BASICGOFASTSPEEDPROFILE_H_

#include <Arduino.h>
#include "SpeedProfile.h"

class BasicGOFastSpeedProfile: public SpeedProfile {
public:
	BasicGOFastSpeedProfile();
	~BasicGOFastSpeedProfile();bool isEnabledContinuousMode();
	byte getTurnSpeed(int percentDone);
	byte get180TurnSpeed(int percentDone);
	byte getFrontSpeed(int percentDone, bool fromContinuous,
			bool continueStraight);

	double getPIDKp();
	double getPIDKi();
	double getPIDKd();
private:
};

#endif /* CONFIG_BASICGOFASTSPEEDPROFILE_H_ */
