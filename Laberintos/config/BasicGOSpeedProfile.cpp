#include "BasicGOSpeedProfile.h"

BasicGOSpeedProfile::BasicGOSpeedProfile() {

}

BasicGOSpeedProfile::~BasicGOSpeedProfile() {

}

byte BasicGOSpeedProfile::get180TurnSpeed(int percentDone) {
	byte speed = 0;
	if (percentDone < 15) {
		speed = 35;
	} else if (percentDone < 40) {
		speed = 60;
	} else if (percentDone < 70) {
		speed = 75;
	} else if (percentDone < 90) {
		speed = 40;
	} else if (percentDone < 100) {
		speed = 30;
	}
	return speed;
}

byte BasicGOSpeedProfile::getTurnSpeed(int percentDone) {
	byte speed = 0;
	if (percentDone < 15) {
		speed = 35;
	} else if (percentDone < 40) {
		speed = 60;
	} else if (percentDone < 70) {
		speed = 75;
	} else if (percentDone < 90) {
		speed = 40;
	} else if (percentDone < 100) {
		speed = 30;
	}
	return speed;
}

byte BasicGOSpeedProfile::getFrontSpeed(int percentDone,
bool fromContinuous, bool continueStraight) {
	byte speed = 0;
	if (percentDone < 15 && !(fromContinuous && isEnabledContinuousMode())) {
		speed = 35;
	} else if (percentDone < 60
			|| (continueStraight && isEnabledContinuousMode()
					&& percentDone < 100)) {
		speed = 50;
	} else if (percentDone < 85) {
		speed = 45;
	} else if (percentDone < 100) {
		speed = 32;
	} else {
		if (DEBUG_ENABLED) {
			Serial.println("fin por 100%");
		}
		speed = 0;
	}
	return speed;
}

bool BasicGOSpeedProfile::isEnabledContinuousMode() {
	return true;
}

double BasicGOSpeedProfile::getPIDKp() {
	return 5.0;
}

double BasicGOSpeedProfile::getPIDKi() {
	return 0.5;
}

double BasicGOSpeedProfile::getPIDKd() {
	return 1.0;
}
