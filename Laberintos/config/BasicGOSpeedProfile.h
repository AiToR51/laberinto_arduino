/*
 * profile.h
 *
 *  Created on: 8 nov. 2017
 *      Author: AGR
 */

#ifndef CONFIG_BASICGOSPEEDPROFILE_H_
#define CONFIG_BASICGOSPEEDPROFILE_H_

#include <Arduino.h>
#include "SpeedProfile.h"

class BasicGOSpeedProfile: public SpeedProfile {
public:
	BasicGOSpeedProfile();
	~BasicGOSpeedProfile();bool isEnabledContinuousMode();
	byte getTurnSpeed(int percentDone);
	byte get180TurnSpeed(int percentDone);
	byte getFrontSpeed(int percentDone, bool fromContinuous,
			bool continueStraight);

	double getPIDKp();
	double getPIDKi();
	double getPIDKd();
private:
};

#endif /* CONFIG_BASICGOSPEEDPROFILE_H_ */
