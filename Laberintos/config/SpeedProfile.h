/*
 * profile.h
 *
 *  Created on: 8 nov. 2017
 *      Author: AGR
 */

#ifndef CONFIG_SPEEDPROFILE_H_
#define CONFIG_SPEEDPROFILE_H_

#include <Arduino.h>
#define DEBUG_ENABLED false

class SpeedProfile {
public:
	virtual bool isEnabledContinuousMode() = 0;
	virtual byte getTurnSpeed(int percentDone) = 0;
	virtual byte get180TurnSpeed(int percentDone) = 0;
	/**
	 * continueStraight indica si es posible que haya que seguir recto */
	virtual byte getFrontSpeed(int percentDone, bool fromContinuous, bool continueStraight) = 0;

	virtual double getPIDKp() = 0;
	virtual double getPIDKi() = 0;
	virtual double getPIDKd() = 0;
private:
};



#endif /* CONFIG_SPEEDPROFILE_H_ */
